/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (c) 2020-2023, Arm Limited
 */

#ifndef ARM_TSTEE_H
#define ARM_TSTEE_H

/*
 * Extend the TEE driver implementation ID list defined in
 * include/uapi/linux/tee.h
 */
#define TEE_IMPL_ID_TSTEE 3

#endif /* ARM_TSTEE_H */
