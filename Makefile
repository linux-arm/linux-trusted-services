# SPDX-License-Identifier: GPL-2.0-only

ARCH			:= arm64
CROSS_COMPILE		?= aarch64-linux-gnu-

ROOT			?= $(CURDIR)/..
KDIR			?= $(ROOT)/linux
TARGET_DIR		?= $(ROOT)/shared
BUILD_DIR		?= $(CURDIR)/build
BUILD_DIR_MAKEFILE 	?= $(BUILD_DIR)/Makefile

all: module

clean: module-clean

install: all
	cp $(BUILD_DIR)/arm-tstee.ko $(TARGET_DIR)/
	cp load_module.sh $(TARGET_DIR)/

module: $(BUILD_DIR_MAKEFILE)
	$(MAKE) -C $(KDIR) M=$(BUILD_DIR) src=$(CURDIR) modules \
		ARCH=$(ARCH) CROSS_COMPILE="$(CROSS_COMPILE)"

module-clean:
	$(MAKE) -C $(KDIR) M=$(BUILD_DIR) src=$(CURDIR) clean \
		ARCH=$(ARCH) CROSS_COMPILE="$(CROSS_COMPILE)"
	rm $(BUILD_DIR_MAKEFILE)

$(BUILD_DIR):
	mkdir -p "$@"

$(BUILD_DIR_MAKEFILE): $(BUILD_DIR)
	touch "$@"
